file(GLOB_RECURSE sources CONFIGURE_DEPENDS
    src/*.c
    src/*.h
    include/*.h
)

add_executable(image-transformer ${sources}   "include/image.h"     "include/bmp_header.h" "src/bmpIO.c"   "include/rotate.h" "src/rotate.c" "src/image-transformer.c")
target_include_directories(image-transformer PRIVATE src include)
