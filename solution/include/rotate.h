#include <stdint.h>
#include <stdio.h>
#ifndef ROTATE_H_INCLUDED
#define ROTATE_H_INCLUDED

struct image rotate(const struct image* image);

#endif
