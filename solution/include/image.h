#include <stdint.h>
#include <stdio.h>
#ifndef IMAGE_H_INCLUDED
#define IMAGE_H_INCLUDED

struct image {
	uint64_t width, height;
	struct pixel* data;
};

struct pixel { uint8_t b, g, r; };


#endif
