#ifndef BMPIO_H_INCLUDED
#define BMPIO_H_INCLUDED
#include "bmp_header.h"
#include "image.h"
#include <stdbool.h>
#include <stdint.h>
#include <stdio.h>

enum read_status {
	READ_OK = 0,
	READ_INVALID_SIGNATURE,
	READ_INVALID_BITS,
	READ_INVALID_HEADER
};

bool read_bmp_header(FILE* file, struct bmp_header* header);

bool read_bmp_body(FILE* file, struct bmp_header* header, struct image* image);

enum read_status from_bmp(FILE* file, struct image* image, struct bmp_header* header);

enum write_status {
	WRITE_OK = 0,
	WRITE_INVALID_SIGNATURE,
	WRITE_INVALID_BITS,
	WRITE_INVALID_HEADER
};

bool write_bmp_header(FILE* file, struct bmp_header* header);

bool write_bmp_body(FILE* file, struct bmp_header* header, struct image* image);

enum write_status to_bmp(FILE* file, struct image* image, struct bmp_header* header);

void overwrite_header(struct bmp_header* header, struct image* image);

#endif
