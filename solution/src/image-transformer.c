#include "bmpIO.h"
#include "bmp_header.h"
#include "image.h"
#include "rotate.h"
#include <stdio.h>
#include <stdlib.h>


int main(int argc, char** argv) {
    (void)argc; (void)argv; // supress 'unused parameters' warning
    if (argc != 3) {
        printf("Invalid number of arguments");
        return 0;
    }
    FILE* source_file = fopen(argv[1], "rb");
    if (source_file == NULL) {
        printf("File not found");
        return 0;
    }

    FILE* transformed_file = fopen(argv[2], "wb");

    struct image source_image;
    struct image transformed_image;
    struct bmp_header header;

    switch (from_bmp(source_file, &source_image, &header)) {
    case READ_OK:
    {
        printf("File read successfully");
        break;
    }
    case READ_INVALID_SIGNATURE:
    {
        printf("File is not bmp");
        return READ_INVALID_SIGNATURE;
    }
    case READ_INVALID_BITS:
    {
        printf("Read bits error");
        return READ_INVALID_BITS;
    }
    case READ_INVALID_HEADER:
    {
        printf("Read header error");
        return READ_INVALID_HEADER;
    }
    default:
        break;
    }

    transformed_image = rotate(&source_image);
    overwrite_header(&header, &transformed_image);


    switch (to_bmp(transformed_file, &transformed_image, &header)) {
    case WRITE_OK:
    {
        printf("File write successfully");
        break;
    }
    case WRITE_INVALID_SIGNATURE:
    {
        printf("File is not bmp");
        return WRITE_INVALID_SIGNATURE;
    }
    case WRITE_INVALID_BITS:
    {
        printf("Write bits error");
        return WRITE_INVALID_BITS;
    }
    case WRITE_INVALID_HEADER:
    {
        printf("Write header error");
        return WRITE_INVALID_HEADER;
    }
    default:
        break;
    }
    fclose(source_file);
    fclose(transformed_file);
    free(source_image.data);
    free(transformed_image.data);
    return 0;
}
