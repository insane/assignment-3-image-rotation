#include "bmpIO.h"
#include "image.h"
#include "rotate.h"
#include <stdlib.h>


struct image rotate(const struct image* image) {
	struct image rotated_image;
	rotated_image.width = image->height;
	rotated_image.height = image->width;
	struct pixel* data = malloc(sizeof(struct pixel) * rotated_image.width * rotated_image.height);
	for (uint64_t i = 0; i < rotated_image.height; i++) {
		for (uint64_t j = 0; j < rotated_image.width; j++) {
			data[i * rotated_image.width + j] = image->data[(image->height - j - 1) * image->width + i];
		}
	}
	rotated_image.data = data;
	return rotated_image;
}
