#include "bmpIO.h"
#include "bmp_header.h"
#include "image.h"
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>




static uint32_t calculate_padding(uint32_t width) {
	const uint32_t multiplicity = 4;
	const uint32_t byte_multiplier = 3;
	return multiplicity - (byte_multiplier * width) % multiplicity;
}

bool read_bmp_header(FILE* file, struct bmp_header* header) {
	return fread(header, sizeof(struct bmp_header), 1, file) == 1;
}

bool read_bmp_body(FILE* file, struct bmp_header* header, struct image* image) {
	fseek(file, header->bOffBits, SEEK_SET);
	image->width = header->biWidth;
	image->height = header->biHeight;
	image->data = malloc(sizeof(struct pixel) * header->biWidth * header->biHeight);
	uint32_t padding = calculate_padding(header->biWidth);

	for (uint64_t i = 0; i < header->biHeight; i++) {
		if (fread(image->data + i * header->biWidth, sizeof(struct pixel), header->biWidth, file) != header->biWidth) {
			return false;
		}
		fseek(file, padding, SEEK_CUR);
	}
	return true;
}

bool write_bmp_header(FILE* file, struct bmp_header* header) {
	return fwrite(header, sizeof(struct bmp_header), 1, file) == 1;
}

bool write_bmp_body(FILE* file, struct bmp_header* header, struct image* image) {
	fseek(file, header->bOffBits, SEEK_SET);
	uint32_t padding = calculate_padding(header->biWidth);

	for (uint64_t i = 0; i < header->biHeight; i++) {
		if (fwrite(image->data + i * header->biWidth, sizeof(struct pixel), header->biWidth, file) != header->biWidth) {
			return false;
		}
		fseek(file, padding, SEEK_CUR);
	}
	return true;
}

enum read_status from_bmp(FILE* file, struct image* image, struct bmp_header* header) {
	//if (header->bfType != 19778) { return READ_INVALID_SIGNATURE; }
	if (!read_bmp_header(file, header)) { return READ_INVALID_HEADER; }
	if (!read_bmp_body(file, header, image)) { return READ_INVALID_BITS; }
	return READ_OK;

}

enum write_status to_bmp(FILE* file, struct image* image, struct bmp_header* header) {
	//if (header->bfType != 19778) { return WRITE_INVALID_SIGNATURE; }
	if(!write_bmp_header(file, header)) { return WRITE_INVALID_HEADER; }
	if (!write_bmp_body(file, header, image)) { return WRITE_INVALID_BITS; }
	return WRITE_OK;
}

void overwrite_header(struct bmp_header* header, struct image* image) {
	header->biWidth = image->width;
	header->biHeight = image->height;	
}
